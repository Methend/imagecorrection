﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ImageCorrection
{
    public partial class ImageFilteringForm : Form
    {
        MainForm Ownerform;
        ImageFiltering imageFiltering;
        
        public ImageFilteringForm(MainForm ownerForm)
        {
            this.Ownerform = ownerForm;
            imageFiltering = new ImageFiltering();
            InitializeComponent();
            for (double i = 0.1; i < 0.5; i += 0.1)
            {
                noiseComboBox.Items.Add(Math.Round(i, 1));
            }
            for (int i = 1; i < 8; i++)
            {
                medianFilterValuecomboBox.Items.Add(i);
            }
        }

        private void createImageNoiseButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = imageFiltering.getImageWithNoise((Bitmap)Ownerform.pictureBoxWithChangedImage.Image, returnValueOfNoise());
        }
        
        private double returnValueOfNoise()
        {
            if (noiseComboBox.SelectedItem == null)
            {
                return 0;
            }
            else
            {
                return (double)noiseComboBox.SelectedItem;
            }
        }

        private int returnValueOfSize()
        {
            if (medianFilterValuecomboBox.SelectedItem == null)
            {
                return 0;
            }
            else
            {
                return (int)medianFilterValuecomboBox.SelectedItem;
            }
        }

        private void correctImageWithLineFilterButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image =  imageFiltering.getImageCorrectedWithLineFilter((Bitmap)Ownerform.pictureBoxWithChangedImage.Image);
        }
      
        private void correctImageWithMedianFilterButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = imageFiltering.getImageCorrectedWithMedianFilter((Bitmap)Ownerform.pictureBoxWithChangedImage.Image, returnValueOfSize());
        }

        private void cloneOriginalImageButton(object sender, EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                Ownerform.pictureBoxWithChangedImage.Image = Ownerform.pictureBoxWithOriginalImage.Image.Clone() as Image;
            }
        }

        private void closeFormButton(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}



