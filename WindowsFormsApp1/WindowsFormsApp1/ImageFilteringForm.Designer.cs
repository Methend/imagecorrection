﻿namespace ImageCorrection
{
    partial class ImageFilteringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.noiseComboBox = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.medianFilterValuecomboBox = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(183, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 21);
            this.button1.TabIndex = 0;
            this.button1.Text = "Применить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.createImageNoiseButton);
            // 
            // noiseComboBox
            // 
            this.noiseComboBox.FormattingEnabled = true;
            this.noiseComboBox.Location = new System.Drawing.Point(12, 24);
            this.noiseComboBox.Name = "noiseComboBox";
            this.noiseComboBox.Size = new System.Drawing.Size(121, 21);
            this.noiseComboBox.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, -2);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "Вероятность";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 60);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "Линейный фильтр";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(183, 60);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Применить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.correctImageWithLineFilterButton);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(12, 86);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 5;
            this.textBox3.Text = "Медианный фильтр";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(183, 110);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(98, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "Применить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.correctImageWithMedianFilterButton);
            // 
            // medianFilterValuecomboBox
            // 
            this.medianFilterValuecomboBox.FormattingEnabled = true;
            this.medianFilterValuecomboBox.Location = new System.Drawing.Point(12, 112);
            this.medianFilterValuecomboBox.Name = "medianFilterValuecomboBox";
            this.medianFilterValuecomboBox.Size = new System.Drawing.Size(121, 21);
            this.medianFilterValuecomboBox.TabIndex = 7;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(41, 154);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(92, 36);
            this.button4.TabIndex = 8;
            this.button4.Text = "Исходное изображение";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.cloneOriginalImageButton);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(183, 154);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(76, 36);
            this.button5.TabIndex = 11;
            this.button5.Text = "ОК";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.closeFormButton);
            // 
            // ImageFilteringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 202);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.medianFilterValuecomboBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.noiseComboBox);
            this.Controls.Add(this.button1);
            this.Name = "ImageFilteringForm";
            this.Text = "Шум и фильтры";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox noiseComboBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox medianFilterValuecomboBox;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}