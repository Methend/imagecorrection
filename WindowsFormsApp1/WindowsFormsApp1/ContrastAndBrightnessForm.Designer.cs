﻿namespace ImageCorrection
{
    partial class ContrastAndBrightnessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.brightnessTextBox = new System.Windows.Forms.TextBox();
            this.contrastTextBox = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.brightnessTrackBar = new System.Windows.Forms.TrackBar();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.contrastTrackBar = new System.Windows.Forms.TrackBar();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.brightnessTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contrastTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.brightnessTextBox.Location = new System.Drawing.Point(121, 10);
            this.brightnessTextBox.Name = "textBox1";
            this.brightnessTextBox.Size = new System.Drawing.Size(56, 20);
            this.brightnessTextBox.TabIndex = 0;
            this.brightnessTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.contrastTextBox.Location = new System.Drawing.Point(121, 61);
            this.contrastTextBox.Name = "textBox2";
            this.contrastTextBox.Size = new System.Drawing.Size(56, 20);
            this.contrastTextBox.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(8, 164);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 2;
            // 
            // trackBar1
            // 
            this.brightnessTrackBar.Location = new System.Drawing.Point(207, 10);
            this.brightnessTrackBar.Name = "trackBar1";
            this.brightnessTrackBar.Size = new System.Drawing.Size(88, 45);
            this.brightnessTrackBar.TabIndex = 3;
            this.brightnessTrackBar.Scroll += new System.EventHandler(this.brightnessBar_Scroll);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(324, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Применить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.changeBrightnessButton);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(121, 112);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 36);
            this.button2.TabIndex = 5;
            this.button2.Text = "Исходное изображение";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.cloneOriginalImageButton);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(8, 61);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(82, 20);
            this.textBox4.TabIndex = 6;
            this.textBox4.Text = "Контрастность";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(12, 12);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(78, 20);
            this.textBox5.TabIndex = 7;
            this.textBox5.Text = "Яркость";
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // trackBar2
            // 
            this.contrastTrackBar.Location = new System.Drawing.Point(207, 61);
            this.contrastTrackBar.Name = "trackBar2";
            this.contrastTrackBar.Size = new System.Drawing.Size(88, 45);
            this.contrastTrackBar.TabIndex = 8;
            this.contrastTrackBar.Scroll += new System.EventHandler(this.contrastBar_Scroll);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(324, 61);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Применить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.changeContrastButton);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(219, 112);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(76, 36);
            this.button4.TabIndex = 10;
            this.button4.Text = "ОК";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.closeFormButton);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 166);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.contrastTrackBar);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.brightnessTrackBar);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.contrastTextBox);
            this.Controls.Add(this.brightnessTextBox);
            this.Name = "Form2";
            this.Text = "Яркость/Контрастность";
            ((System.ComponentModel.ISupportInitialize)(this.brightnessTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contrastTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox brightnessTextBox;
        private System.Windows.Forms.TextBox contrastTextBox;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TrackBar brightnessTrackBar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TrackBar contrastTrackBar;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}