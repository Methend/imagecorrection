﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ImageCorrection
{
    class ImageFiltering
    {
        Bitmap resultImage;
        public Bitmap getImageWithNoise(Bitmap bmpImg, double size)
        {
            Random randomValue = new Random();
            for (int i = 0; i < bmpImg.Width; i++)
            {
                for (int j = 0; j < bmpImg.Height; j++)
                {
                    Color color = bmpImg.GetPixel(i, j);
                    if (randomValue.NextDouble() < size)
                    {
                        byte r = (byte)(color.R == 1 ? color.R : 255);
                        byte b = (byte)(color.B == 1 ? color.B : 255);
                        byte g = (byte)(color.G == 1 ? color.G : 255);
                        bmpImg.SetPixel(i, j, Color.FromArgb(255, r, g, b));
                    }
                }
            }
            return bmpImg;
        }

        public Bitmap getImageCorrectedWithLineFilter(Bitmap bmpImg)
        {
            int[,] arrR = new int[bmpImg.Width, bmpImg.Height];
            int[,] arrG = new int[bmpImg.Width, bmpImg.Height];
            int[,] arrB = new int[bmpImg.Width, bmpImg.Height];
            resultImage = new Bitmap(bmpImg.Width, bmpImg.Height);
            for (int i = 0; i < bmpImg.Width; i++)
            {
                for (int j = 0; j < bmpImg.Height; j++)
                {
                    arrR[i, j] = bmpImg.GetPixel(i, j).R;
                    arrG[i, j] = bmpImg.GetPixel(i, j).G;
                    arrB[i, j] = bmpImg.GetPixel(i, j).B;
                }
            }
            for (int i = 1; i < bmpImg.Width - 1; i++)
            {
                for (int j = 1; j < bmpImg.Height - 1; j++)
                {
                    int arrRSum = 0, arrGSum = 0, arrBSum = 0;
                    for (int x = -1; x < 2; x++)
                    {
                        for (int y = -1; y < 2; y++)
                        {
                            arrRSum = arrRSum + arrR[i + x, j + y] / 9;
                            arrGSum = arrGSum + arrG[i + x, j + y] / 9;
                            arrBSum = arrBSum + arrB[i + x, j + y] / 9;
                        }
                    }
                    resultImage.SetPixel(i, j, Color.FromArgb(arrRSum, arrGSum, arrBSum));
                }
            }
            return resultImage;
        }

        public Bitmap getImageCorrectedWithMedianFilter(Bitmap bmpImg, int Size)
        {
            Color TempColor;
            try
            {
                for (int x = 0; x < bmpImg.Width; ++x)
                {
                    for (int y = 0; y < bmpImg.Height; ++y)
                    {
                        List<int> RValues = new List<int>();
                        List<int> GValues = new List<int>();
                        List<int> BValues = new List<int>();
                        List<Color> Values = new List<Color>();
                        for (int x2 = 0; x2 < Size; ++x2)
                        {
                            int TempX = x + x2;
                            if (TempX >= 0 && TempX < bmpImg.Width)
                            {
                                for (int y2 = 0; y2 < Size; ++y2)
                                {
                                    int TempY = y + y2;
                                    if (TempY >= 0 && TempY < bmpImg.Height)
                                    {
                                        TempColor = bmpImg.GetPixel(TempX, TempY);
                                        RValues.Add(TempColor.R);
                                        GValues.Add(TempColor.G);
                                        BValues.Add(TempColor.B);
                                        Values.Add(TempColor);
                                    }
                                }
                            }
                        }
                        RValues.Sort();
                        GValues.Sort();
                        BValues.Sort();
                        Color MedianPixel = getSortedBrightnessList(Values);
                        bmpImg.SetPixel(x, y, MedianPixel);
                    }
                }
            }
            catch {
                MessageBox.Show("Невозможно применить фильтр", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return bmpImg;
        }
        private Color getSortedBrightnessList(List<Color> Values)
        {
            Color temp;
            for (int i = 0; i < Values.Count - 1; i++)
            {
                for (int j = i + 1; j < Values.Count; j++)
                {
                    if (255 * Values[i].GetBrightness() > 255 * Values[j].GetBrightness())
                    {
                        temp = Values[i];
                        Values[i] = Values[j];
                        Values[j] = temp;
                    }
                }
            }
            return Values[Values.Count / 2];
        }
    }
}
