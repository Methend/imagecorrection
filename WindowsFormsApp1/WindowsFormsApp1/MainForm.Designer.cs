﻿namespace ImageCorrection
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.graphOfPixelBrightnessOnLine = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.greenColorChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.redColorChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button1 = new System.Windows.Forms.Button();
            this.cursorCoordinatesTextBox = new System.Windows.Forms.TextBox();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.pictureBoxWithOriginalImage = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveButton = new System.Windows.Forms.ToolStripMenuItem();
            this.informaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.фильтрыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.контрастToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.преобразованияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шумToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выделениеГраницToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.цветовыеОттенкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.цветовойБалансToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueColorChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.allColorsChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pictureBoxWithChangedImage = new System.Windows.Forms.PictureBox();
            this.button6 = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.соляризацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.graphOfPixelBrightnessOnLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenColorChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redColorChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWithOriginalImage)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.blueColorChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allColorsChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWithChangedImage)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Left;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.graphOfPixelBrightnessOnLine);
            this.splitContainer1.Panel1.Controls.Add(this.greenColorChart);
            this.splitContainer1.Panel1.Controls.Add(this.redColorChart);
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.cursorCoordinatesTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.statusStrip2);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBoxWithOriginalImage);
            this.splitContainer1.Panel1.Controls.Add(this.menuStrip1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.blueColorChart);
            this.splitContainer1.Panel2.Controls.Add(this.allColorsChart);
            this.splitContainer1.Panel2.Controls.Add(this.pictureBoxWithChangedImage);
            this.splitContainer1.Panel2.Controls.Add(this.button6);
            this.splitContainer1.Size = new System.Drawing.Size(1760, 838);
            this.splitContainer1.SplitterDistance = 752;
            this.splitContainer1.TabIndex = 0;
            // 
            // graphOfPixelBrightnessOnLine
            // 
            chartArea1.Name = "ChartArea1";
            this.graphOfPixelBrightnessOnLine.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.graphOfPixelBrightnessOnLine.Legends.Add(legend1);
            this.graphOfPixelBrightnessOnLine.Location = new System.Drawing.Point(-2, 456);
            this.graphOfPixelBrightnessOnLine.Name = "graphOfPixelBrightnessOnLine";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Color = System.Drawing.Color.Red;
            series1.Legend = "Legend1";
            series1.Name = "R";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series2.Color = System.Drawing.Color.Lime;
            series2.Legend = "Legend1";
            series2.Name = "G";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series3.Color = System.Drawing.Color.Blue;
            series3.Legend = "Legend1";
            series3.Name = "B";
            this.graphOfPixelBrightnessOnLine.Series.Add(series1);
            this.graphOfPixelBrightnessOnLine.Series.Add(series2);
            this.graphOfPixelBrightnessOnLine.Series.Add(series3);
            this.graphOfPixelBrightnessOnLine.Size = new System.Drawing.Size(747, 353);
            this.graphOfPixelBrightnessOnLine.TabIndex = 2;
            this.graphOfPixelBrightnessOnLine.Text = "chart5";
            // 
            // greenColorChart
            // 
            chartArea2.Name = "ChartArea1";
            this.greenColorChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.greenColorChart.Legends.Add(legend2);
            this.greenColorChart.Location = new System.Drawing.Point(444, 270);
            this.greenColorChart.Name = "greenColorChart";
            series4.ChartArea = "ChartArea1";
            series4.Color = System.Drawing.Color.Green;
            series4.LabelForeColor = System.Drawing.Color.Bisque;
            series4.Legend = "Legend1";
            series4.Name = "G";
            this.greenColorChart.Series.Add(series4);
            this.greenColorChart.Size = new System.Drawing.Size(273, 180);
            this.greenColorChart.TabIndex = 14;
            this.greenColorChart.Text = "chart3";
            // 
            // redColorChart
            // 
            chartArea3.Name = "ChartArea1";
            this.redColorChart.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.redColorChart.Legends.Add(legend3);
            this.redColorChart.Location = new System.Drawing.Point(433, 43);
            this.redColorChart.Name = "redColorChart";
            series5.ChartArea = "ChartArea1";
            series5.Color = System.Drawing.Color.Red;
            series5.Legend = "Legend1";
            series5.Name = "R";
            this.redColorChart.Series.Add(series5);
            this.redColorChart.Size = new System.Drawing.Size(273, 180);
            this.redColorChart.TabIndex = 2;
            this.redColorChart.Text = "chart2";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(349, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Гистограмма";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.drawAllHistogramsButton);
            // 
            // cursorCoordinatesTextBox
            // 
            this.cursorCoordinatesTextBox.Location = new System.Drawing.Point(245, 4);
            this.cursorCoordinatesTextBox.Name = "cursorCoordinatesTextBox";
            this.cursorCoordinatesTextBox.ReadOnly = true;
            this.cursorCoordinatesTextBox.Size = new System.Drawing.Size(87, 20);
            this.cursorCoordinatesTextBox.TabIndex = 5;
            // 
            // statusStrip2
            // 
            this.statusStrip2.Location = new System.Drawing.Point(0, 812);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(748, 22);
            this.statusStrip2.TabIndex = 3;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // pictureBoxWithOriginalImage
            // 
            this.pictureBoxWithOriginalImage.Location = new System.Drawing.Point(3, 28);
            this.pictureBoxWithOriginalImage.Name = "pictureBoxWithOriginalImage";
            this.pictureBoxWithOriginalImage.Size = new System.Drawing.Size(384, 422);
            this.pictureBoxWithOriginalImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWithOriginalImage.TabIndex = 1;
            this.pictureBoxWithOriginalImage.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.informaToolStripMenuItem,
            this.фильтрыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(748, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveButton});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.openToolStripMenuItem.Text = "Открыть";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openImageThroughMenu);
            // 
            // saveButton
            // 
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(132, 22);
            this.saveButton.Text = "Сохранить";
            this.saveButton.Click += new System.EventHandler(this.saveImageButton);
            // 
            // informaToolStripMenuItem
            // 
            this.informaToolStripMenuItem.Name = "informaToolStripMenuItem";
            this.informaToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.informaToolStripMenuItem.Text = "Информация";
            this.informaToolStripMenuItem.Click += new System.EventHandler(this.informationAboutImageToolStripMenuItem_Click);
            // 
            // фильтрыToolStripMenuItem
            // 
            this.фильтрыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.контрастToolStripMenuItem,
            this.преобразованияToolStripMenuItem,
            this.шумToolStripMenuItem,
            this.выделениеГраницToolStripMenuItem,
            this.цветовыеОттенкиToolStripMenuItem,
            this.цветовойБалансToolStripMenuItem,
            this.соляризацияToolStripMenuItem});
            this.фильтрыToolStripMenuItem.Name = "фильтрыToolStripMenuItem";
            this.фильтрыToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.фильтрыToolStripMenuItem.Text = "Инструменты";
            // 
            // контрастToolStripMenuItem
            // 
            this.контрастToolStripMenuItem.Name = "контрастToolStripMenuItem";
            this.контрастToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.контрастToolStripMenuItem.Text = "Яркость/Контрастность";
            this.контрастToolStripMenuItem.Click += new System.EventHandler(this.contrastAndBrightnessToolStripMenuItem_Click);
            // 
            // преобразованияToolStripMenuItem
            // 
            this.преобразованияToolStripMenuItem.Name = "преобразованияToolStripMenuItem";
            this.преобразованияToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.преобразованияToolStripMenuItem.Text = "Стандартные Преобразования";
            this.преобразованияToolStripMenuItem.Click += new System.EventHandler(this.standartdImageCorrectionTypesToolStripMenuItem_Click);
            // 
            // шумToolStripMenuItem
            // 
            this.шумToolStripMenuItem.Name = "шумToolStripMenuItem";
            this.шумToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.шумToolStripMenuItem.Text = "Фильтрация";
            this.шумToolStripMenuItem.Click += new System.EventHandler(this.imageFilteringToolStripMenuItem_Click);
            // 
            // выделениеГраницToolStripMenuItem
            // 
            this.выделениеГраницToolStripMenuItem.Name = "выделениеГраницToolStripMenuItem";
            this.выделениеГраницToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.выделениеГраницToolStripMenuItem.Text = "Выделение границ";
            this.выделениеГраницToolStripMenuItem.Click += new System.EventHandler(this.edgeDetectionToolStripMenuItem_Click);
            // 
            // цветовыеОттенкиToolStripMenuItem
            // 
            this.цветовыеОттенкиToolStripMenuItem.Name = "цветовыеОттенкиToolStripMenuItem";
            this.цветовыеОттенкиToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.цветовыеОттенкиToolStripMenuItem.Text = "Цветовые оттенки";
            this.цветовыеОттенкиToolStripMenuItem.Click += new System.EventHandler(this.colourTintToolStripMenuItem_Click);
            // 
            // цветовойБалансToolStripMenuItem
            // 
            this.цветовойБалансToolStripMenuItem.Name = "цветовойБалансToolStripMenuItem";
            this.цветовойБалансToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.цветовойБалансToolStripMenuItem.Text = "Цветовой баланс";
            this.цветовойБалансToolStripMenuItem.Click += new System.EventHandler(this.colourBalanceToolStripMenuItem_Click);
            // 
            // blueColorChart
            // 
            chartArea4.Name = "ChartArea1";
            this.blueColorChart.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.blueColorChart.Legends.Add(legend4);
            this.blueColorChart.Location = new System.Drawing.Point(584, 43);
            this.blueColorChart.Name = "blueColorChart";
            series6.ChartArea = "ChartArea1";
            series6.Color = System.Drawing.Color.Blue;
            series6.Legend = "Legend1";
            series6.Name = "B";
            this.blueColorChart.Series.Add(series6);
            this.blueColorChart.Size = new System.Drawing.Size(273, 180);
            this.blueColorChart.TabIndex = 15;
            this.blueColorChart.Text = "chart4";
            // 
            // allColorsChart
            // 
            chartArea5.Name = "ChartArea1";
            this.allColorsChart.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.allColorsChart.Legends.Add(legend5);
            this.allColorsChart.Location = new System.Drawing.Point(584, 270);
            this.allColorsChart.Name = "allColorsChart";
            series7.BorderColor = System.Drawing.Color.Black;
            series7.ChartArea = "ChartArea1";
            series7.Color = System.Drawing.Color.Black;
            series7.Legend = "Legend1";
            series7.Name = "All";
            series7.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.allColorsChart.Series.Add(series7);
            this.allColorsChart.Size = new System.Drawing.Size(273, 180);
            this.allColorsChart.TabIndex = 14;
            this.allColorsChart.Text = "chart1";
            // 
            // pictureBoxWithChangedImage
            // 
            this.pictureBoxWithChangedImage.Location = new System.Drawing.Point(3, 28);
            this.pictureBoxWithChangedImage.Name = "pictureBoxWithChangedImage";
            this.pictureBoxWithChangedImage.Size = new System.Drawing.Size(518, 422);
            this.pictureBoxWithChangedImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWithChangedImage.TabIndex = 3;
            this.pictureBoxWithChangedImage.TabStop = false;
            this.pictureBoxWithChangedImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.changedImagedPictureBox_MouseDown);
            this.pictureBoxWithChangedImage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.changedImagedPictureBox_MouseUp);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(173, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(192, 21);
            this.button6.TabIndex = 12;
            this.button6.Text = "Получить исходное изображение";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.cloneOriginalImageButton);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Images|*.bmp;*.jpeg";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(1760, 816);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(0, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // соляризацияToolStripMenuItem
            // 
            this.соляризацияToolStripMenuItem.Name = "соляризацияToolStripMenuItem";
            this.соляризацияToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.соляризацияToolStripMenuItem.Text = "Соляризация";
            this.соляризацияToolStripMenuItem.Click += new System.EventHandler(this.solariseToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1660, 838);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.splitContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Приложение для обработки изображений";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.graphOfPixelBrightnessOnLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenColorChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redColorChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWithOriginalImage)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.blueColorChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allColorsChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWithChangedImage)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripMenuItem saveButton;
        private System.Windows.Forms.ToolStripMenuItem informaToolStripMenuItem;
        private System.Windows.Forms.TextBox cursorCoordinatesTextBox;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ToolStripMenuItem фильтрыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem контрастToolStripMenuItem;
        public System.Windows.Forms.PictureBox pictureBoxWithOriginalImage;
        public System.Windows.Forms.PictureBox pictureBoxWithChangedImage;
        private System.Windows.Forms.ToolStripMenuItem преобразованияToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataVisualization.Charting.Chart allColorsChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart redColorChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart greenColorChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart blueColorChart;
        private System.Windows.Forms.ToolStripMenuItem шумToolStripMenuItem;
        private System.Windows.Forms.DataVisualization.Charting.Chart graphOfPixelBrightnessOnLine;
        private System.Windows.Forms.ToolStripMenuItem выделениеГраницToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem цветовыеОттенкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem цветовойБалансToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem соляризацияToolStripMenuItem;
    }
}

