﻿using System.Drawing;

namespace ImageCorrection
{
    class BrightnessCorrection
    {
        Bitmap resultImage;
        public Bitmap getImageWithChangedBrightness(Bitmap bmpImg, int brightness)
        {
            resultImage = new Bitmap(bmpImg.Width, bmpImg.Height);
            Color color;
            for (int i = 0; i < bmpImg.Width; i++)
            {
                for (int j = 0; j < bmpImg.Height; j++)
                {
                    color = bmpImg.GetPixel(i, j);
                    int cR = color.R + brightness;
                    int cG = color.G + brightness;
                    int cB = color.B + brightness;
                    if (cR < 0) cR = 1;
                    if (cR > 255) cR = 255;

                    if (cG < 0) cG = 1;
                    if (cG > 255) cG = 255;

                    if (cB < 0) cB = 1;
                    if (cB > 255) cB = 255;

                    resultImage.SetPixel(i, j,
        Color.FromArgb((byte)cR, (byte)cG, (byte)cB));
                }
            }
            return resultImage;
        }

    }
}
