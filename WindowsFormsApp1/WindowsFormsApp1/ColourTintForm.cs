﻿using System.Drawing;
using System.Windows.Forms;

namespace ImageCorrection
{
    public partial class ColourTintForm : Form
    {
        MainForm Ownerform;
        ColourTint colorTint;
        public ColourTintForm(MainForm ownerForm)
        {
            this.Ownerform = ownerForm;
            colorTint = new ColourTint();
            InitializeComponent();
            gColorTrackBar.Minimum = bColorTrackBar.Minimum = rColorTrackBar.Minimum = 0;
            gColorTrackBar.Maximum = bColorTrackBar.Maximum = rColorTrackBar.Maximum = 100;
        }

        private void rColorTrackBar_Scroll(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                rColorTextBox.Text = "" + rColorTrackBar.Value;
                Ownerform.pictureBoxWithChangedImage.Image = colorTint.getImageCorrectedWithColorTint((Bitmap)Ownerform.pictureBoxWithOriginalImage.Image, bColorTrackBar.Value, gColorTrackBar.Value, rColorTrackBar.Value);
            }
        }

        private void gColorTrackBar_Scroll(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                gColorTextBox.Text = "" + gColorTrackBar.Value;
                Ownerform.pictureBoxWithChangedImage.Image = colorTint.getImageCorrectedWithColorTint((Bitmap)Ownerform.pictureBoxWithOriginalImage.Image, bColorTrackBar.Value, gColorTrackBar.Value, rColorTrackBar.Value);
            }
        }

        private void bColorTrackBar_Scroll(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                bColorTextBox.Text = "" + bColorTrackBar.Value;
                Ownerform.pictureBoxWithChangedImage.Image = colorTint.getImageCorrectedWithColorTint((Bitmap)Ownerform.pictureBoxWithOriginalImage.Image, bColorTrackBar.Value, gColorTrackBar.Value, rColorTrackBar.Value);
            }
        }

        private void cloneOriginalImageButton(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                Ownerform.pictureBoxWithChangedImage.Image = Ownerform.pictureBoxWithOriginalImage.Image.Clone() as Image;
            }
        }

        private void closeFormButton(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
