﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ImageCorrection
{
    public partial class EdgeDetectionForm : Form
    {
        MainForm Ownerform;
        EdgeDetection edgeDetection;
        public EdgeDetectionForm(MainForm ownerForm)
        {
            this.Ownerform = ownerForm;
            edgeDetection = new EdgeDetection();
            InitializeComponent();
        }


        private void convertImageWithKirshAndLaplacianButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = edgeDetection.getImageCorrectedWithEdgeDetection((Bitmap)Ownerform.pictureBoxWithChangedImage.Image, EdgeDetection.Kirsch3x3Horizontal, EdgeDetection.Laplacian3x3, true);
        }
        private void convertImageWithSobelButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = edgeDetection.getImageCorrectedWithEdgeDetection((Bitmap)Ownerform.pictureBoxWithChangedImage.Image, EdgeDetection.Sobel3x3Horizontal, EdgeDetection.Sobel3x3Vertical, true);
        }

        private void convertImageWithLaplacianButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = edgeDetection.getImageCorrectedWithConvolutionFilter((Bitmap)Ownerform.pictureBoxWithChangedImage.Image, EdgeDetection.Laplacian3x3, 1.0, 0, true);
        }

        private void convertImageWithWallaceButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = edgeDetection.getImageCorrectedWithConvolutionFilter((Bitmap)Ownerform.pictureBoxWithChangedImage.Image, EdgeDetection.Wallace3x3, 1.0, -500, true);
        }
        private void convertImageWithRobertDetectionButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = edgeDetection.getImageCorrectedWithRobertDetection((Bitmap)Ownerform.pictureBoxWithChangedImage.Image);
        }

        private void cloneOriginalImageButton(object sender, EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                Ownerform.pictureBoxWithChangedImage.Image = Ownerform.pictureBoxWithOriginalImage.Image.Clone() as Image;
            }
        }

        private void closeFormButton(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
