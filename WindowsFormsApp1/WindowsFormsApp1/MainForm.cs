﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ImageCorrection
{
    public partial class MainForm : Form
    {
        ContrastAndBrightnessForm contrastAndBrightnessForm;
        StandardImageCorrectionWaysForm standardImageCorrectionTypesForm;
        ImageFilteringForm imageFilteringForm;
        EdgeDetectionForm edgeDetectionForm;
        ColourTintForm colourTintForm;
        ColourBalanceForm colourBalanceForm;
        SolariseForm solariseForm;
        List<Chart> allCharts;
        Point firstPointOfLine = new Point();
        Point secondPointOfLine = new Point();

        public MainForm()
        {
            InitializeComponent();
            allCharts = new List<Chart>();
            Axis axOfGraph = new Axis();
            Axis ayOfGraph = new Axis();
            axOfGraph.Title = "Расстояние";
            ayOfGraph.Title = "Яркость";
            graphOfPixelBrightnessOnLine.ChartAreas[0].AxisX = axOfGraph;
            graphOfPixelBrightnessOnLine.ChartAreas[0].AxisY = ayOfGraph;
            Axis axOfHistogram = new Axis();
            Axis ayOfHistogram = new Axis();
            Axis axOfBlueColorChart = new Axis();
            Axis ayOfBlueColorChart = new Axis();
            axOfHistogram.Title = "Значение пикселей";
            ayOfHistogram.Title = "Количество пикселей";
            axOfBlueColorChart.Title = axOfHistogram.Title;
            ayOfBlueColorChart.Title = ayOfHistogram.Title;
            allColorsChart.ChartAreas[0].AxisX = axOfHistogram; allColorsChart.ChartAreas[0].AxisY = ayOfHistogram;
            redColorChart.ChartAreas[0].AxisX = axOfHistogram; redColorChart.ChartAreas[0].AxisY = ayOfHistogram;
            greenColorChart.ChartAreas[0].AxisX = axOfHistogram; greenColorChart.ChartAreas[0].AxisY = ayOfHistogram;
            blueColorChart.ChartAreas[0].AxisX = axOfBlueColorChart; blueColorChart.ChartAreas[0].AxisY = ayOfBlueColorChart;
            allCharts.Add(redColorChart); allCharts.Add(greenColorChart); allCharts.Add(blueColorChart); allCharts.Add(allColorsChart);
        }

        private void openImageThroughMenu(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pictureBoxWithOriginalImage.Image = new Bitmap(openFileDialog.FileName);
                pictureBoxWithChangedImage.Image = new Bitmap(openFileDialog.FileName);
            }
            pictureBoxWithOriginalImage.MouseMove += setCursorCoordinates;
            pictureBoxWithChangedImage.SizeMode = PictureBoxSizeMode.Zoom;
            drawAllHistograms((Bitmap)pictureBoxWithChangedImage.Image);
        }

        private void saveImageButton(object sender, EventArgs e)
        {
            if (pictureBoxWithOriginalImage.Image != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Сохранить картинку как...";
                saveFileDialog.OverwritePrompt = true;
                saveFileDialog.CheckPathExists = true;
                saveFileDialog.Filter = "Image Files(.bmp)| *.bmp|Image Files(.jpeg)| *.jpeg|All files(*.*)|*.*";
                saveFileDialog.ShowHelp = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        pictureBoxWithChangedImage.Image.Save(saveFileDialog.FileName);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
        }

        public string getFileExtension(string fileName)
        {
            return fileName.Substring(fileName.LastIndexOf(".") + 1);
        }

        private void informationAboutImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pictureBoxWithOriginalImage.Image == null)
            {
                MessageBox.Show("Данных о изображении нет", "Заголовок");
            }
            else
            {
                string height = pictureBoxWithOriginalImage.Height.ToString();
                string width = pictureBoxWithOriginalImage.Width.ToString();
                string format = getFileExtension(openFileDialog.FileName);
                string heightAndWidth = "Высота и ширина изображения: ";
                string formatText = "Формат изображения: ";
                MessageBox.Show(heightAndWidth + height + " " + width + "\n" + formatText + format, "Информация о изображении");
            }

        }

        private void setCursorCoordinates(object sender, MouseEventArgs mouseEventArg)
        {
            Image originalImage = pictureBoxWithOriginalImage.Image;
            int xCoordinate = originalImage.Width * mouseEventArg.X / pictureBoxWithOriginalImage.Width;
            int yCoordinate = originalImage.Height * mouseEventArg.Y / pictureBoxWithOriginalImage.Height;
            cursorCoordinatesTextBox.Text = String.Format("X={0}, Y={1}", xCoordinate, yCoordinate);
        }

        private void cloneOriginalImageButton(object sender, EventArgs e)
        {
            if (pictureBoxWithOriginalImage.Image != null)
            {
                pictureBoxWithChangedImage.Image = pictureBoxWithOriginalImage.Image.Clone() as Image;
            }
        }


        private void contrastAndBrightnessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contrastAndBrightnessForm == null || contrastAndBrightnessForm.IsDisposed)
            {
                contrastAndBrightnessForm = new ContrastAndBrightnessForm(this);
                contrastAndBrightnessForm.Show();
            }
        }

        private void standartdImageCorrectionTypesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (standardImageCorrectionTypesForm == null || standardImageCorrectionTypesForm.IsDisposed)
            {
                standardImageCorrectionTypesForm = new StandardImageCorrectionWaysForm(this);
                standardImageCorrectionTypesForm.Show();
            }
        }

        public void drawAllHistograms(Bitmap bmpImg)
        {
            if (pictureBoxWithOriginalImage.Image == null) { return; }
            allCharts.ForEach(chart =>
            {
                chart.Series[0].Points.Clear();
            });
            int[] brigh_on_point = new int[256];
            int[] rcolor = new int[256];
            int[] gcolor = new int[256];
            int[] bcolor = new int[256];
            for (int i = 0; i < bmpImg.Width; i++)
            {
                for (int j = 0; j < bmpImg.Height; j++)
                {
                    int s = (int)(255 * bmpImg.GetPixel(i, j).GetBrightness());
                    int r = (bmpImg.GetPixel(i, j).R);
                    int g = (bmpImg.GetPixel(i, j).G);
                    int b = (bmpImg.GetPixel(i, j).B);
                    brigh_on_point[s]++;
                    rcolor[r]++;
                    gcolor[g]++;
                    bcolor[b]++;
                }
            }

            for (int s = 0; s < 256; s++)
            {
                redColorChart.Series[0].Points.AddXY(s, rcolor[s]);
                blueColorChart.Series[0].Points.AddXY(s, bcolor[s]);
                greenColorChart.Series[0].Points.AddXY(s, gcolor[s]);
                allColorsChart.Series[0].Points.AddXY(s, brigh_on_point[s]);
            }
        }

        private void drawAllHistogramsButton(object sender, EventArgs e)
        {
            drawAllHistograms((Bitmap)pictureBoxWithChangedImage.Image);
        }

        private void imageFilteringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                if (imageFilteringForm == null || imageFilteringForm.IsDisposed)
                {
                    imageFilteringForm = new ImageFilteringForm(this);
                    imageFilteringForm.Show();
                }
            }
        }

        private void changedImagedPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                firstPointOfLine = e.Location;
        }

        private void changedImagedPictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            Pen penLine = new Pen(Color.Blue, 1);
            if (e.Button == MouseButtons.Left)
            {
                secondPointOfLine = e.Location;
                Graphics graphics = Graphics.FromImage((Bitmap)pictureBoxWithChangedImage.Image);
                graphics.DrawLine(penLine, firstPointOfLine, secondPointOfLine);
                Console.WriteLine(graphics.DpiX);
                Console.WriteLine(graphics.DpiY);
                pictureBoxWithChangedImage.Invalidate();
                graphics.Dispose();
                DrawGraph((Bitmap)pictureBoxWithChangedImage.Image);
                Refresh();
            }
        }

        public void DrawGraph(Bitmap bmpImg)
        {
            graphOfPixelBrightnessOnLine.Series[0].Points.Clear();
            graphOfPixelBrightnessOnLine.Series[1].Points.Clear();
            graphOfPixelBrightnessOnLine.Series[2].Points.Clear();
            int size = (int)Math.Round(Math.Sqrt(Math.Pow((secondPointOfLine.X - firstPointOfLine.X), 2) + Math.Pow((secondPointOfLine.Y - firstPointOfLine.Y), 2)), 2);
            graphOfPixelBrightnessOnLine.ChartAreas[0].AxisX.Maximum = size;
            graphOfPixelBrightnessOnLine.ChartAreas[0].AxisX.Minimum = 0;
            int[,] lengthr = new int[size + 1, size + 1];
            int[,] lengthg = new int[size + 1, size + 1];
            int[,] lengthb = new int[size + 1, size + 1];
            try
            {
                if ((firstPointOfLine.X < secondPointOfLine.X) && (firstPointOfLine.Y < secondPointOfLine.Y))
                {
                    for (int i = firstPointOfLine.X - 1, j = firstPointOfLine.Y - 1; (i < secondPointOfLine.X + 1 && j < secondPointOfLine.Y + 1); i++, j++)
                    {
                        lengthr[i, j] = (bmpImg.GetPixel(i, j).R);
                        lengthb[i, j] = (bmpImg.GetPixel(i, j).B);
                        lengthg[i, j] = (bmpImg.GetPixel(i, j).G);
                    }
                    for (int i = firstPointOfLine.X, j = firstPointOfLine.Y, s = 0; (i < secondPointOfLine.X && j < secondPointOfLine.Y && s < lengthr.Length); i++, j++, s++)
                    {
                        graphOfPixelBrightnessOnLine.Series[0].Points.AddXY(s, lengthr[i, j]);
                        graphOfPixelBrightnessOnLine.Series[1].Points.AddXY(s, lengthg[i, j]);
                        graphOfPixelBrightnessOnLine.Series[2].Points.AddXY(s, lengthb[i, j]);
                    }
                }
                if ((firstPointOfLine.Y > secondPointOfLine.Y) && (firstPointOfLine.X < secondPointOfLine.X))
                {
                    for (int i = firstPointOfLine.X, j = secondPointOfLine.Y; (i < secondPointOfLine.X && j < firstPointOfLine.Y); i++, j++)
                    {
                        lengthr[i, j] = (bmpImg.GetPixel(i, j).R);
                        lengthb[i, j] = (bmpImg.GetPixel(i, j).B);
                        lengthg[i, j] = (bmpImg.GetPixel(i, j).G);
                    }
                    for (int i = firstPointOfLine.X, j = secondPointOfLine.Y, s = 0; (i < secondPointOfLine.X && j < firstPointOfLine.Y && s < lengthr.Length); i++, j++, s++)
                    {
                        graphOfPixelBrightnessOnLine.Series[0].Points.AddXY(s, lengthr[i, j]);
                        graphOfPixelBrightnessOnLine.Series[1].Points.AddXY(s, lengthg[i, j]);
                        graphOfPixelBrightnessOnLine.Series[2].Points.AddXY(s, lengthb[i, j]);
                    }

                }
                if ((firstPointOfLine.Y > secondPointOfLine.Y) && (firstPointOfLine.X > secondPointOfLine.X))
                {
                    for (int i = secondPointOfLine.X, j = secondPointOfLine.Y; (i < firstPointOfLine.X && j < firstPointOfLine.Y); i++, j++)
                    {
                        lengthr[i, j] = (bmpImg.GetPixel(i, j).R);
                        lengthb[i, j] = (bmpImg.GetPixel(i, j).B);
                        lengthg[i, j] = (bmpImg.GetPixel(i, j).G);
                    }
                    for (int i = secondPointOfLine.X, j = secondPointOfLine.Y, s = 0; (i < firstPointOfLine.X && j < firstPointOfLine.Y && s < lengthr.Length); i++, j++, s++)
                    {
                        graphOfPixelBrightnessOnLine.Series[0].Points.AddXY(s, lengthr[i, j]);
                        graphOfPixelBrightnessOnLine.Series[1].Points.AddXY(s, lengthg[i, j]);
                        graphOfPixelBrightnessOnLine.Series[2].Points.AddXY(s, lengthb[i, j]);
                    }

                }
                if ((firstPointOfLine.Y < secondPointOfLine.Y) && (firstPointOfLine.X > secondPointOfLine.X))
                {
                    for (int i = secondPointOfLine.X, j = firstPointOfLine.Y; (i < firstPointOfLine.X && j < secondPointOfLine.Y); i++, j++)
                    {
                        lengthr[i, j] = (bmpImg.GetPixel(i, j).R);
                        lengthb[i, j] = (bmpImg.GetPixel(i, j).B);
                        lengthg[i, j] = (bmpImg.GetPixel(i, j).G);
                    }
                    for (int i = secondPointOfLine.X, j = firstPointOfLine.Y, s = 0; (i < firstPointOfLine.X && j < secondPointOfLine.Y && s < lengthr.Length); i++, j++, s++)
                    {
                        graphOfPixelBrightnessOnLine.Series[0].Points.AddXY(s, lengthr[i, j]);
                        graphOfPixelBrightnessOnLine.Series[1].Points.AddXY(s, lengthg[i, j]);
                        graphOfPixelBrightnessOnLine.Series[2].Points.AddXY(s, lengthb[i, j]);
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void edgeDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (edgeDetectionForm == null || edgeDetectionForm.IsDisposed)
            {
                edgeDetectionForm = new EdgeDetectionForm(this);
                edgeDetectionForm.Show();
            }
        }

        private void colourTintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colourTintForm == null || colourTintForm.IsDisposed)
            {
                colourTintForm = new ColourTintForm(this);
                colourTintForm.Show();
            }
        }

        private void colourBalanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colourBalanceForm == null || colourBalanceForm.IsDisposed)
            {
                colourBalanceForm = new ColourBalanceForm(this);
                colourBalanceForm.Show();
            }
        }

        private void solariseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (solariseForm == null || solariseForm.IsDisposed)
            {
                solariseForm = new SolariseForm(this);
                solariseForm.Show();
            }
        }
    }
}
