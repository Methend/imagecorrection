﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ImageCorrection
{
    public partial class ContrastAndBrightnessForm : Form
    {

        MainForm Ownerform;
        ContrastCorrection contrastCorrection;
        BrightnessCorrection brightnessCorrection;
        public ContrastAndBrightnessForm(MainForm ownerForm)
        {
            this.Ownerform = ownerForm;
            contrastCorrection = new ContrastCorrection();
            brightnessCorrection = new BrightnessCorrection();
            InitializeComponent();
            brightnessTrackBar.Maximum = 255;
            brightnessTrackBar.Minimum = -255;
            contrastTrackBar.Maximum = 255;
            contrastTrackBar.Minimum = 0;
        }

        
        private void changeBrightnessButton(object sender, EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                Ownerform.pictureBoxWithChangedImage.Image = brightnessCorrection.getImageWithChangedBrightness((Bitmap)Ownerform.pictureBoxWithChangedImage.Image, brightnessTrackBar.Value);
            }
        }

        private void brightnessBar_Scroll(object sender, EventArgs e)
        {

            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                brightnessTextBox.Text = "" + brightnessTrackBar.Value;
            }
        }

        private void cloneOriginalImageButton(object sender, EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                Ownerform.pictureBoxWithChangedImage.Image = Ownerform.pictureBoxWithOriginalImage.Image.Clone() as Image;
            }
        }

        private void contrastBar_Scroll(object sender, EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                contrastTextBox.Text = "" + contrastTrackBar.Value;
            }
        }

        private void changeContrastButton(object sender, EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                Ownerform.pictureBoxWithChangedImage.Image = contrastCorrection.getImageWithChangedContrast((Bitmap)Ownerform.pictureBoxWithChangedImage.Image, contrastTrackBar.Value);
            }
        }

        private void closeFormButton(object sender, EventArgs e)
        {
            this.Close();
        }
    }

   
}
