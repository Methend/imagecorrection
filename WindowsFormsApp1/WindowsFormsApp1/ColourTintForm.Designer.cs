﻿namespace ImageCorrection
{
    partial class ColourTintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gColorTrackBar = new System.Windows.Forms.TrackBar();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.rColorTrackBar = new System.Windows.Forms.TrackBar();
            this.gColorTextBox = new System.Windows.Forms.TextBox();
            this.rColorTextBox = new System.Windows.Forms.TextBox();
            this.bColorTrackBar = new System.Windows.Forms.TrackBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.bColorTextBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gColorTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rColorTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bColorTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // gColorTrackBar
            // 
            this.gColorTrackBar.Location = new System.Drawing.Point(182, 61);
            this.gColorTrackBar.Name = "gColorTrackBar";
            this.gColorTrackBar.Size = new System.Drawing.Size(186, 45);
            this.gColorTrackBar.TabIndex = 15;
            this.gColorTrackBar.Scroll += new System.EventHandler(this.gColorTrackBar_Scroll);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(12, 12);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(34, 20);
            this.textBox5.TabIndex = 14;
            this.textBox5.Text = "R";
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(12, 61);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(34, 20);
            this.textBox4.TabIndex = 13;
            this.textBox4.Text = "G";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rColorTrackBar
            // 
            this.rColorTrackBar.Location = new System.Drawing.Point(182, 10);
            this.rColorTrackBar.Name = "rColorTrackBar";
            this.rColorTrackBar.Size = new System.Drawing.Size(186, 45);
            this.rColorTrackBar.TabIndex = 12;
            this.rColorTrackBar.Scroll += new System.EventHandler(this.rColorTrackBar_Scroll);
            // 
            // gColorTextBox
            // 
            this.gColorTextBox.Location = new System.Drawing.Point(81, 61);
            this.gColorTextBox.Name = "gColorTextBox";
            this.gColorTextBox.Size = new System.Drawing.Size(56, 20);
            this.gColorTextBox.TabIndex = 11;
            // 
            // rColorTextBox
            // 
            this.rColorTextBox.Location = new System.Drawing.Point(81, 12);
            this.rColorTextBox.Name = "rColorTextBox";
            this.rColorTextBox.Size = new System.Drawing.Size(56, 20);
            this.rColorTextBox.TabIndex = 10;
            this.rColorTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bColorTrackBar
            // 
            this.bColorTrackBar.Location = new System.Drawing.Point(182, 112);
            this.bColorTrackBar.Name = "bColorTrackBar";
            this.bColorTrackBar.Size = new System.Drawing.Size(186, 45);
            this.bColorTrackBar.TabIndex = 19;
            this.bColorTrackBar.Scroll += new System.EventHandler(this.bColorTrackBar_Scroll);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 112);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(34, 20);
            this.textBox1.TabIndex = 18;
            this.textBox1.Text = "B";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bColorTextBox
            // 
            this.bColorTextBox.Location = new System.Drawing.Point(81, 112);
            this.bColorTextBox.Name = "bColorTextBox";
            this.bColorTextBox.Size = new System.Drawing.Size(56, 20);
            this.bColorTextBox.TabIndex = 17;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(81, 168);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 36);
            this.button2.TabIndex = 20;
            this.button2.Text = "Исходное изображение";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.cloneOriginalImageButton);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(213, 168);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(76, 36);
            this.button4.TabIndex = 21;
            this.button4.Text = "ОК";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.closeFormButton);
            // 
            // ColourTintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 235);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bColorTrackBar);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.bColorTextBox);
            this.Controls.Add(this.gColorTrackBar);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.rColorTrackBar);
            this.Controls.Add(this.gColorTextBox);
            this.Controls.Add(this.rColorTextBox);
            this.Name = "ColourTintForm";
            this.Text = "ColourTintForm";
            ((System.ComponentModel.ISupportInitialize)(this.gColorTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rColorTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bColorTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TrackBar gColorTrackBar;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TrackBar rColorTrackBar;
        private System.Windows.Forms.TextBox gColorTextBox;
        private System.Windows.Forms.TextBox rColorTextBox;
        private System.Windows.Forms.TrackBar bColorTrackBar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox bColorTextBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
    }
}