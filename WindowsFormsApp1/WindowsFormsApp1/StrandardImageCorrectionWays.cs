﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ImageCorrection
{
    class StrandardImageCorrectionWays
    {
        Bitmap resultImage;
        public Bitmap getConvertedImageToBinary(Bitmap bmpImg, int binarizationThreshold)
        {
            resultImage = new Bitmap(bmpImg.Width, bmpImg.Height);
            try
            {
                for (int j = 0; j < bmpImg.Height; j++)
                {
                    for (int i = 0; i < bmpImg.Width; i++)
                    {
                        Color color = bmpImg.GetPixel(i, j);
                        int avgPixel = (color.R + color.G + color.B) / 3;
                        resultImage.SetPixel(i, j, avgPixel <= binarizationThreshold ? Color.Black : Color.White);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return resultImage;
        }

        public Bitmap getConvertedImageToGrayScale(Bitmap bmpImg)
        {
            resultImage = new Bitmap(bmpImg.Width, bmpImg.Height);
            try
            {
                for (int j = 0; j < bmpImg.Height; j++)
                {
                    for (int i = 0; i < bmpImg.Width; i++)
                    {
                        Color color = bmpImg.GetPixel(i, j);
                        byte grayPixel = (byte)(.299 * color.R + .587 * color.G + .114 * color.B);
                        resultImage.SetPixel(i, j, Color.FromArgb(grayPixel, grayPixel, grayPixel));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return resultImage;
        }

        public Bitmap getConvertedImageToInvert(Bitmap bmpImg)
        {
            resultImage = new Bitmap(bmpImg.Width, bmpImg.Height);
            try
            {
                for (int j = 0; j < bmpImg.Height; j++)
                {
                    for (int i = 0; i < bmpImg.Width; i++)
                    {
                        Color color = bmpImg.GetPixel(i, j);
                        resultImage.SetPixel(i, j, Color.FromArgb(255 - color.R, 255 - color.G, 255 - color.B));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return resultImage;
        }
    }
}
