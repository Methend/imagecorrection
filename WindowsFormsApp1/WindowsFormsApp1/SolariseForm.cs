﻿using System.Drawing;
using System.Windows.Forms;

namespace ImageCorrection
{
    public partial class SolariseForm : Form
    {
        MainForm Ownerform;
        Solarise solarise;
        public SolariseForm(MainForm ownerForm)
        {
            this.Ownerform = ownerForm;
            solarise = new Solarise();
            InitializeComponent();
            gColorTrackBar.Minimum = bColorTrackBar.Minimum = rColorTrackBar.Minimum = 0;
            gColorTrackBar.Maximum = bColorTrackBar.Maximum = rColorTrackBar.Maximum = 250;
        }

        private void rColorTrackBar_Scroll(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                rColorTextBox.Text = "" + rColorTrackBar.Value;
                Ownerform.pictureBoxWithChangedImage.Image = solarise.getImageCorrectedWithSolarise((Bitmap)Ownerform.pictureBoxWithOriginalImage.Image, (byte)bColorTrackBar.Value, (byte)gColorTrackBar.Value, (byte)rColorTrackBar.Value);
            }
        }

        private void gColorTrackBar_Scroll(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                gColorTextBox.Text = "" + gColorTrackBar.Value;
                Ownerform.pictureBoxWithChangedImage.Image = solarise.getImageCorrectedWithSolarise((Bitmap)Ownerform.pictureBoxWithOriginalImage.Image, (byte)bColorTrackBar.Value, (byte)gColorTrackBar.Value, (byte)rColorTrackBar.Value);
            }
        }

        private void bColorTrackBar_Scroll(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                bColorTextBox.Text = "" + bColorTrackBar.Value;
                Ownerform.pictureBoxWithChangedImage.Image = solarise.getImageCorrectedWithSolarise((Bitmap)Ownerform.pictureBoxWithOriginalImage.Image, (byte)bColorTrackBar.Value, (byte)gColorTrackBar.Value, (byte)rColorTrackBar.Value);
            }
        }

        private void cloneOriginalImageButton(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                Ownerform.pictureBoxWithChangedImage.Image = Ownerform.pictureBoxWithOriginalImage.Image.Clone() as Image;
            }
        }

        private void closeFormButton(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
