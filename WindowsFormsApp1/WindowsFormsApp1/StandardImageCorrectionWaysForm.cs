﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ImageCorrection
{
    public partial class StandardImageCorrectionWaysForm : Form
    {
        MainForm Ownerform;
        StrandardImageCorrectionWays strandardImageCorrectionWays;
        public StandardImageCorrectionWaysForm(MainForm ownerForm)
        {
            this.Ownerform = ownerForm;
            strandardImageCorrectionWays = new StrandardImageCorrectionWays();
            InitializeComponent();
        }

        private void convertImageToBinaryButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = strandardImageCorrectionWays.getConvertedImageToBinary((Bitmap)Ownerform.pictureBoxWithChangedImage.Image, 100);
        }

        private void convertImageToGrayScaleButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = strandardImageCorrectionWays.getConvertedImageToGrayScale((Bitmap)Ownerform.pictureBoxWithChangedImage.Image);
        }

        private void convertImageToInvertButton(object sender, EventArgs e)
        {
            Ownerform.pictureBoxWithChangedImage.Image = strandardImageCorrectionWays.getConvertedImageToInvert((Bitmap)Ownerform.pictureBoxWithChangedImage.Image);
        }

        private void cloneOriginalImageButton(object sender, EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                Ownerform.pictureBoxWithChangedImage.Image = Ownerform.pictureBoxWithOriginalImage.Image.Clone() as Image;
            }
        }

        private void closeFormButton(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
