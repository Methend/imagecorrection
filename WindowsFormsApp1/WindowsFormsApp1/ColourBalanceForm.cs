﻿using System.Drawing;
using System.Windows.Forms;

namespace ImageCorrection
{
    public partial class ColourBalanceForm : Form
    {
        ColourBalance colourBalance;
        MainForm Ownerform;
        public ColourBalanceForm(MainForm ownerForm)
        {
            this.Ownerform = ownerForm;
            colourBalance = new ColourBalance();
            InitializeComponent();
            gColorTrackBar.Minimum = bColorTrackBar.Minimum = rColorTrackBar.Minimum = 0;
            gColorTrackBar.Maximum = bColorTrackBar.Maximum = rColorTrackBar.Maximum = 250;
            gColorTrackBar.Value = bColorTrackBar.Value = rColorTrackBar.Value = 125;
        }

        private void rColorTrackBar_Scroll(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                rColorTextBox.Text = "" + rColorTrackBar.Value;
                Ownerform.pictureBoxWithChangedImage.Image = colourBalance.getImageCorrectedWithColorBalance((Bitmap)Ownerform.pictureBoxWithOriginalImage.Image, (byte)bColorTrackBar.Value, (byte)gColorTrackBar.Value, (byte)rColorTrackBar.Value);
            }
        }

        private void gColorTrackBar_Scroll(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                gColorTextBox.Text = "" + gColorTrackBar.Value;
                Ownerform.pictureBoxWithChangedImage.Image = colourBalance.getImageCorrectedWithColorBalance((Bitmap)Ownerform.pictureBoxWithOriginalImage.Image, (byte)bColorTrackBar.Value, (byte)gColorTrackBar.Value, (byte)rColorTrackBar.Value);
            }
        }

        private void bColorTrackBar_Scroll(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                bColorTextBox.Text = "" + bColorTrackBar.Value;
                Ownerform.pictureBoxWithChangedImage.Image = colourBalance.getImageCorrectedWithColorBalance((Bitmap)Ownerform.pictureBoxWithOriginalImage.Image, (byte)bColorTrackBar.Value, (byte)gColorTrackBar.Value, (byte)rColorTrackBar.Value);
            }
        }

        private void cloneOriginalImageButton(object sender, System.EventArgs e)
        {
            if (Ownerform.pictureBoxWithOriginalImage.Image != null)
            {
                Ownerform.pictureBoxWithChangedImage.Image = Ownerform.pictureBoxWithOriginalImage.Image.Clone() as Image;
            }
        }

        private void closeFormButton(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
