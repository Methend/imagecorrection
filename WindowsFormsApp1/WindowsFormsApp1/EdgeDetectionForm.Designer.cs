﻿namespace ImageCorrection
{
    partial class EdgeDetectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(195, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 20);
            this.button1.TabIndex = 0;
            this.button1.Text = "Применить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.convertImageWithKirshAndLaplacianButton);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(27, 27);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 4;
            this.textBox2.Text = "Метод Кирша";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(27, 65);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 5;
            this.textBox1.Text = "Метод Собеля";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(195, 65);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 20);
            this.button2.TabIndex = 6;
            this.button2.Text = "Применить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.convertImageWithSobelButton);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(27, 100);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 7;
            this.textBox3.Text = "Метод Лапласа";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(195, 99);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(139, 20);
            this.button3.TabIndex = 8;
            this.button3.Text = "Применить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.convertImageWithLaplacianButton);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(27, 135);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 9;
            this.textBox4.Text = "Метод Уоллеса";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(195, 135);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(139, 20);
            this.button4.TabIndex = 10;
            this.button4.Text = "Применить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.convertImageWithWallaceButton);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(27, 171);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 11;
            this.textBox5.Text = "Метод Роберта";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(195, 171);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(139, 20);
            this.button5.TabIndex = 12;
            this.button5.Text = "Применить";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.convertImageWithRobertDetectionButton);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(35, 209);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(92, 36);
            this.button6.TabIndex = 13;
            this.button6.Text = "Исходное изображение";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.cloneOriginalImageButton);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(195, 209);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(76, 36);
            this.button7.TabIndex = 14;
            this.button7.Text = "ОК";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.closeFormButton);
            // 
            // EdgeDetectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 257);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button1);
            this.Name = "EdgeDetectionForm";
            this.Text = "Выделение границ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
    }
}