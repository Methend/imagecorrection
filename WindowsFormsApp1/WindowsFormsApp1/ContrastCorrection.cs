﻿using System.Drawing;

namespace ImageCorrection
{
    class ContrastCorrection
    {
        Bitmap resultImage;
        public Bitmap getImageWithChangedContrast(Bitmap bmpImg, double contrastValue)
        {
            resultImage = new Bitmap(bmpImg.Width, bmpImg.Height);
            contrastValue = (100.0 + contrastValue) / 100.0;
            contrastValue *= contrastValue;
            for (int i = 0; i < bmpImg.Width; i++)
            {
                for (int j = 0; j < bmpImg.Height; j++)
                {
                    Color color = bmpImg.GetPixel(i, j);
                    double pR = getContrastPixel(contrastValue, color.R);
                    double pG = getContrastPixel(contrastValue, color.G);
                    double pB = getContrastPixel(contrastValue, color.B);
                    resultImage.SetPixel(i, j, Color.FromArgb((byte)pR, (byte)pG, (byte)pB));
                }
            }
            return resultImage;
        }

        private double getContrastPixel(double contrastValue, byte color)
        {
            double pixel = color / 255.0;
            pixel -= 0.5;
            pixel *= contrastValue;
            pixel += 0.5;
            pixel *= 255;
            if (pixel < 0) return 0;
            if (pixel > 255) return 255;
            return pixel;
        }
    }
}
